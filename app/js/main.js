$(function(){

	var isMobile = false;
	var width = $(window).width();
	var hash = window.location.hash;

	$assets = {
		allPanels : $('.accordian > .accordianContent'),
		accordianLink : $('.accordian-heading a'),
		browserWindow : $(window),
		bodyHTML : $('body,html')
	}

	$assets.browserWindow.resize(function() {
		var width = $assets.browserWindow.width();
		mobileCheck(width);
	});

	mobileCheck(width);

	if (hash){
		openNewSeasame(hash, 600);
	}

	$assets.accordianLink.on('click', function(e){
		var divToOpen = $(this).attr('href');
		var targetDiv = $(this).parent().next();

		if(!isMobile){
			
			if ( $(e.target).is('.active') ){
				closeAccordion(divToOpen, 600);
			} else {
				closeOtherAccordions(600);
				openNewSeasame(divToOpen, 600);
			}

		} else {
			mobileCloseAccordion();
			openNewSeasame(divToOpen, 600);	
		}

	});

	function mobileCheck(size){
		if (size <= 640){
			isMobile = true;
		} else {
			isMobile = false;
		}
	}

	function mobileCloseAccordion(){
		$assets.allPanels.removeClass('open').attr('style','');
		$assets.accordianLink.removeClass('active');		
	}

	function closeAccordion(target, speed){
		$('a[href=' + target + ']').removeClass('active');
		$(target).slideUp(speed).removeClass('open');		
	}

	function closeOtherAccordions(speed){
		$assets.allPanels.slideUp(speed).removeClass('open');
		$assets.accordianLink.removeClass('active');		
	}

	function openNewSeasame(target, speed){
		$('a[href=' + target + ']').addClass('active');
		$(target).slideDown(speed).addClass('open');
		scrollToTop(target);
	}

	function scrollToTop(anchorPoint){
		$assets.bodyHTML.animate({
			scrollTop: $(anchorPoint).offset().top - 140
		}, 600);
	}


});
