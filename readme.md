# Accordian Demo

Can be run by opening app/index.html in a browser of your choice.

Using gulp to run a quick server for debugging / future proofing.

    $ npm install
    $ gulp

The application will be running on [http://localhost:9999](http://localhost:9999)


## Robert Skwiat
## 908.421.3839

#### Changelog 4.14.2015

##### Visual Changes
* Typography is now 13px, with line height of 17px
* Border is no longer doubled up on bottom / top of panels
* Added WAI attribute roles to HTML panels
* Updates to responsive CSS

##### Functionality Changes
* Mobile dectection now run on page load
* Smoother collapse functionality
* If open panel is clicked - this will now cause the panel to close
* Panels are now fully responsive
    * A user brings their screen in to mobile view, clicks to a new panel, that panel will be the open panel on bringing screen to desktop or tablet view